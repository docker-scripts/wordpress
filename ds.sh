update_network_aliases() {
    local aliases='' domain file
    if [[ -n "$(ls apache2/sites-enabled)" ]]; then
        for file in $(ls apache2/sites-enabled); do
            domain=${file%.conf}
            aliases+=" --alias $domain"
        done
    fi
    docker network disconnect --force $NETWORK $CONTAINER
    [[ -n $aliases ]] \
        && docker network connect $aliases $NETWORK $CONTAINER
}
