cmd_site_help() {
    cat <<_EOF
    site init <domain>
    site install <domain>
    site clone <domain> <new-domain>
    site del <domain>

_EOF
}

cmd_site() {
    local cmd=$1
    local domain=$2
    [[ -z $domain ]] && fail "Usage:\n$(cmd_site_help)\n"

    case $cmd in
        init|install|del)
            _${cmd}_site $domain
            ;;
        clone)
            local new_domain=$3
            _clone_site $domain $new_domain
            ;;
        *)
            fail "Usage:\n$(cmd_site_help)\n"
            ;;
    esac
}

_init_site() {
    local domain=$1

    # create the directory
    [[ -d $domain ]] && fail "Directory '$domain' already exists. Remove it first and try again."
    mkdir -p $domain

    # create 'settings.sh'
    local dbname=${domain//./_}
    local dbuser="$dbname"
    local dbpass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w16 | head -n1)
    local admin_pass=$(tr -cd '[:alnum:]' < /dev/urandom | fold -w16 | head -n1)
    cat <<EOF > $domain/settings.sh
#!/bin/bash

DOMAIN="$domain"

# database
DBHOST="${DBHOST:-mariadb}"
DBPORT="${DBPORT:-3306}"
DBNAME="$dbname"
DBUSER="$dbuser"
DBPASS="$dbpass"

# admin
ADMIN_USER="admin"
ADMIN_PASS="$admin_pass"
ADMIN_EMAIL="${ADMIN_EMAIL:-admin@example.org}"

# site
TITLE="WordPress"
THEME="${THEME:-twentytwentyfive}"
PLUGINS="
  akismet
  all-in-one-wp-migration
  bbpress
  classic-editor
  contact-form-7
  elementor
  jetpack
  updraftplus
  wpforms-lite
  wp-mail-smtp
  wordpress-seo
  woocommerce
"
EOF

    # create .htaccess
    cat <<'EOF' > $domain/.htaccess
# BEGIN WordPress
# If you uncomment IfModule directive you must set AllowOverride All
# 
RewriteEngine On
RewriteBase /
RewriteRule ^index.php$ - [L]
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule . /index.php [L]
# 
# END WordPress
EOF
    
    # create apache config
    cat <<EOF > apache2/sites-available/$domain.conf
<VirtualHost *:80>
        ServerName $domain
        RedirectPermanent / https://$domain/
</VirtualHost>

<VirtualHost _default_:443>
        ServerName $domain

        DocumentRoot /host/$domain
        <Directory /host/$domain/>
            AllowOverride All
            Require all granted
        </Directory>

        SSLEngine on
        SSLCertificateFile  /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key

        <FilesMatch "\.(cgi|shtml|phtml|php)$">
                        SSLOptions +StdEnvVars
        </FilesMatch>
</VirtualHost>
EOF
    ln -s ../sites-available/$domain.conf apache2/sites-enabled/
    ds exec systemctl restart apache2

    # setup the domain on revproxy
    ds @revproxy domains-add $domain
    ds @revproxy get-ssl-cert $domain
    update_network_aliases
    ds @revproxy restart
}

_install_site() {
    local domain=$1
    local wp="ds wp $domain"

    # create a database
    set -a
    source $domain/settings.sh
    set +a
    ds mariadb create

    ds exec chown www-data: -R /host/$domain
    $wp core download

    # create 'wp-config.php'
    $wp core config \
        --dbname="$DBNAME" \
        --dbuser="$DBUSER" \
        --dbpass="$DBPASS" \
        --dbhost="$DBHOST"

    # install wp
    $wp core install \
        --url="https://$DOMAIN" \
        --title="$TITLE" \
        --admin_user="$ADMIN_USER" \
        --admin_password="$ADMIN_PASS" \
        --admin_email="$ADMIN_EMAIL" \
        --skip-email

    # install plugins
    [[ -n $PLUGINS ]] && $wp plugin install $PLUGINS --activate

    # theme
    [[ -n $THEME ]] && $wp theme install $THEME --activate
    
    # update
    $wp core update
    $wp core update-db
    $wp plugin update --all
    #$wp theme update --all

    ds exec systemctl restart apache2
}

_clone_site() {
    local domain=$1
    local new_domain=$2

    [[ -z $new_domain ]] && fail "Usage:\n$(cmd_site_help)\n"
    [[ -d $domain ]] || fail "Directory '$domain' does not exist."
    [[ -d $new_domain ]] && fail "Directory '$new_domain' already exists."
    set -x

    # initialize the new domain
    ds site init $new_domain

    # create a database
    set -a
    source $new_domain/settings.sh
    set +a
    ds mariadb create

    # copy all the files from the current site to the new one
    cp $new_domain/settings.sh $new_domain-settings.sh
    cp -a $domain/* $new_domain/
    mv $new_domain-settings.sh $new_domain/settings.sh
    ds exec chown www-data: -R /host/$new_domain

    # create 'wp-config.php'
    rm $new_domain/wp-config.php
    ds wp $new_domain core config \
        --dbname="$DBNAME" \
        --dbuser="$DBUSER" \
        --dbpass="$DBPASS" \
        --dbhost="$DBHOST"

    # transfer the content of the database
    ds wp $domain db export - > $domain.sql
    ds wp $new_domain db import /host/$domain.sql
    rm $domain.sql

    # fix the 'home' and 'siteurl' options
    ds wp $new_domain option set home "https://$new_domain"
    ds wp $new_domain option set siteurl "https://$new_domain"
    ds wp $new_domain cache flush

    # correct the ADMIN_PASS and ADMIN_EMAIL of the new site
    source $domain/settings.sh
    sed -i $new_domain/settings.sh \
        -e "/ADMIN_PASS/ c ADMIN_PASS='$ADMIN_PASS'" \
        -e "/ADMIN_EMAIL/ c ADMIN_EMAIL='$ADMIN_EMAIL'"

    # restart apache2
    ds exec systemctl restart apache2
}

_del_site() {
    local domain=$1

    # remove revproxy config
    ds @revproxy domains-rm $domain
    update_network_aliases

    # remove apache2 config
    rm -f apache2/sites-enabled/$domain.conf
    rm -f apache2/sites-available/$domain.conf
    ds exec systemctl restart apache2

    # remove the directory
    cp $domain/settings.sh $domain-settings.sh
    rm -rf $domain

    # drop the database
    set -a
    source $domain-settings.sh
    set +a
    ds mariadb drop
    rm $domain-settings.sh

    exit 0
}
