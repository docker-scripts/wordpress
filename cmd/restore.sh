cmd_restore_help() {
    cat <<_EOF
    backup <domain> <backup-file.tgz>
        Restore site from the given backup file.

_EOF
}

cmd_restore() {
    # get the domain
    local domain=$1
    test -n $domain || fail "Usage:\n$(cmd_restore_help)\n"

    # get the backup file
    local file=$2
    test -f "$file" || fail "Usage:\n$(cmd_restore_help)\n"
    local dir=${file%%.tgz}
    [[ $file != $dir ]] || fail "Usage:\n$(cmd_restore_help)\n"

    # extract the backup archive
    tar --extract --gunzip --file=$file
    dir=$(basename $dir)

    # restore wordpress files
    rm -rf $domain
    mv $dir/$domain .
    ds exec chown www-data: -R $domain

    # restore the database
    set -a
    source $domain/settings.sh
    set +a
    ds mariadb drop
    ds mariadb create
    ds wp $domain db import /host/$dir/$domain.sql
    
    # clean up
    rm -rf $dir/
}
